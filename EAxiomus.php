<?php
/**
 * User: mnb
 * Date: 15.10.14
 * Time: 9:03
 */

require_once __DIR__ . '/vendor/Axiomus.php';
use Axiomus\Axiomus;

/**
 * Class EAxiomus
 *
 * @property string $ukey
 * @property string $uid
 */
class EAxiomus extends CComponent {

    /**
     * @var Axiomus
     */
    private $_axiomus;

    public function init() {
    }

    public function __set($name, $value) {
        if (property_exists($this->getAxiomus(), $name)) {
            return $this->getAxiomus()->$name = $value;
        }

        return parent::__set($name, $value);
    }

    public function __get($name) {
        if (property_exists($this->getAxiomus(), $name)) {
            return $this->getAxiomus()->$name;
        }

        return parent::__get($name);
    }

    public function getAxiomus() {
        if (!$this->_axiomus) {
            $this->_axiomus = Axiomus::instance();
        }

        return $this->_axiomus;
    }

    public function createRequest($method = Axiomus::METHOD_NEW, $data = array()) {
        return Axiomus::create($method, $data);
    }

}