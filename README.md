Для установки потребуется так же PHP версия АПИ Аксиомуса. Взять можно [тут](https://bitbucket.org/MNBuyskih/axiomus). Разместить содержимое репозитория нужно в папке `vendor`

Пример конфигурации в `protected/config/main.php`

	return array(
		//...
		'components' => array(
			//...
			'axiomus' => array(
				'uid' => 'Ваш UID',
				'ukey' => 'Ваш UKEY',
				'test' => false, // По умолчанию test = true, 
								 // поэтому переопределите этот параметр обязательно
				'setCache' => function($key, $value, $expire){
					// Метод для сохранения в кеше ответов от аксиомуса.
					// Можно использовать кеш Yii
					return Yii::app()->cache->set($key, $value, $expire);
				},
				'getCache' => function($key){
					// Метод для получения из кеша ответов от аксиомуса.
					return Yii::app()->cache->get($key);
				}
			)
			//...
		)
		//...
	);
